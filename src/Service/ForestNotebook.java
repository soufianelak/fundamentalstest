package Service;

import entities.plant_entities.Plant;
import entities.plant_entities.animal_entities.Animal;
import entities.plant_entities.animal_entities.Carnivore;
import entities.plant_entities.animal_entities.Herbivore;
import entities.plant_entities.animal_entities.Omnivore;

import java.util.*;

public class ForestNotebook {

    private List<Carnivore> carnivores=new ArrayList<>();
    private List<Omnivore> omnivores=new ArrayList<>();
    private List<Herbivore> herbivores=new ArrayList<>();
    private int plantCount;
    private int animalcount;
    private List<Animal> animals=new ArrayList<>();
    private List<Plant> plants=new ArrayList<>();

    public ForestNotebook() {
    }

    public List<Carnivore> getCarnivores() {
        return carnivores;
    }

    public List<Omnivore> getOmnivores() {

        return omnivores;
    }

    public List<Herbivore> getHerbivores() {
        return herbivores;
    }

    public int getPlantCount() {

        plantCount=plantCount+1;
        return plantCount;
    }

    public int getAnimalcount() {
        animalcount=animalcount+1;
        return animalcount;
    }

    public void setCarnivores(List<Carnivore> carnivores) {
        this.carnivores = carnivores;

    }

    public void setOmnivores(List<Omnivore> omnivores) {
        this.omnivores = omnivores;
    }

    public void setHerbivores(List<Herbivore> herbivores) {
        this.herbivores = herbivores;
    }

    public void addAnimals(Animal a) {

        animals.add(a);
        getAnimalcount();
    }

    public void addPlant(Plant p) {

        plants.add(p);
        getPlantCount();

    }

    public void printNotebook() {

        System.out.println("\nList of plants:");
        for (Plant n: plants){
            System.out.println("Name: "+n.getName()+" Height:"+n.getHeight());
        }

        System.out.println("\nList of animals:");

        for (Animal a: animals){
            System.out.println("Name: "+a.getName()+" Weight: "+a.getWeight()+" Height: "+a.getHeight()+" Length: "+a.getLength());
        }


    }

    public void sortAnimalsByName() {

        Collections.sort(animals, new Comparator<Animal>() {
            @Override
            public int compare(Animal o1, Animal o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });


    }

    public void sortPlantsByName() {

        Collections.sort(plants, new Comparator<Plant>() {
            @Override
            public int compare(Plant o1, Plant o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

    }


}
