package app;

import Service.ForestNotebook;
import entities.plant_entities.Plant;
import entities.plant_entities.animal_entities.Animal;
import entities.plant_entities.animal_entities.Omnivore;

import java.util.List;

public class natureApp {

    public static void main(String[] args) {

        ForestNotebook notebook=new ForestNotebook();

        Plant plant = new Plant("Capucine",4);
        Plant plant1=new Plant("Tulipe",3.5);
        Plant plant2=new Plant("Sativa",1.2);
        Plant plant3=new Plant("Juniper compacta",2.5);
        Plant plant4=new Plant("Apple tree",2.5);


        notebook.addPlant(plant);
        notebook.addPlant(plant1);
        notebook.addPlant(plant2);
        notebook.addPlant(plant3);
        notebook.addPlant(plant4);



        Animal animal=new Animal("Snake",50,0.2,3);
        Animal animal1=new Animal("Bear",130,2,1.5);
        Animal animal2=new Animal("Horse",160,1.9,2.5);
        Animal animal3=new Animal("Dog",30,1,1.5);
        Animal animal4=new Animal("Lion",90,1.5,2);
        Animal animal5=new Animal("Cow",200,1.8,2.3);
        Animal animal6=new Animal("Pork",80,1,1.5);
        Animal animal7=new Animal("Bird",4,0.3,0.6);
        Animal animal8=new Animal("Fox",35,0.9,1.4);

        notebook.addAnimals(animal);
        notebook.addAnimals(animal1);
        notebook.addAnimals(animal2);
        notebook.addAnimals(animal3);
        notebook.addAnimals(animal4);
        notebook.addAnimals(animal5);
        notebook.addAnimals(animal6);
        notebook.addAnimals(animal7);
        notebook.addAnimals(animal8);


        System.out.println("You have "+(notebook.getPlantCount()-1)+" Plants and "+(notebook.getAnimalcount()-1)+" Animals");

        notebook.printNotebook();
        notebook.sortPlantsByName();
        notebook.sortAnimalsByName();
        notebook.printNotebook();





    }

}
